﻿function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

window.onclick = function (event) {
    if (!event.target.matches('.dropbtn')) {

        var dropdown = document.getElementById("myDropdown");
        if (dropdown != null)
            dropdown.classList.remove("show");
    }
}