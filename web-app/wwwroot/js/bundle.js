function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

window.onclick = function (event) {
    if (!event.target.matches('.dropbtn')) {

        var dropdown = document.getElementById("myDropdown");
        if (dropdown != null)
            dropdown.classList.remove("show");
    }
}
$(function () {
    $("#add-category").click(function (event) {
        $(".hidden-input").remove();
        $("#newItem").val("");
        $(".form-popup h1").html("Add new category");
        $("#newItem").attr("placeholder", "Name your menu category");
        $(".form-popup").css("display", "block");
        $(".background").css("display", "block"); 
    })
    $("#cancelAdding").click(function (event) {
        $("#newItem").val("");
        $(".hidden-input").remove();
        $(".form-popup").css("display", "none");
        $(".background").css("display", "none");
        $('a').prop('disabled', false);
    })
    $("#addCategory").click(function (event) {     
        let count = $(".category").length;
        $(".form-popup").css("display", "none");
        $(".background").css("display", "none");
        $('a').prop('disabled', false);
    })
    $("#add-subcategory").click(function (event) {
        $("#newItem").val("");
        $(".form-popup h1").html("Add new subcategory");
        $("#newItem").attr("placeholder", "Name your menu subcategory");
        $(".form-popup").css("display", "block");
        $(".background").css("display", "block");

    })
})
$(function () {
    var comments_amount;
    var comments_shown = 0;
    UpdateCommentsSection();

    $(".text-input").click(function () {
        $(".submit").removeClass("disabled");
    });

    $("#false-post").click(function () {
        $(".pop-up-overlay.for-login").removeClass("disabled");
    });

    $("#false-like").click(function () {
        $(".pop-up-overlay.for-login").removeClass("disabled");
    });

    $("#false-dislike").click(function () {
        $(".pop-up-overlay.for-login").removeClass("disabled");
    });

    $(".comments-menu").on('click', "#false-comment", function () {
        $(".pop-up-overlay.for-login").removeClass("disabled");
    });

    $(".pop-up-close").click(function () {
        $(".pop-up-overlay.for-login").addClass("disabled");
    });

    $("textarea").each(function () {
        this.setAttribute("style", "height:" + (this.scrollHeight) + "px;overflow-y:hidden;");
    }).on("input", function () {
        this.style.height = "auto";
        this.style.height = (this.scrollHeight) + "px";
    });

    $(".comments-block").on('click', "#show-more", function (event) {
        comments_shown += 10;
        event.preventDefault();
        $(".a-comment").slice(comments_shown).removeClass("disabled");
        $("#show-more").addClass("disabled");
        $("#show-less").removeClass("disabled");
    });

    $(".comments-block").on('click', "#show-less", function (event) {
        comments_shown -= 10;
        event.preventDefault();
        $(".a-comment").slice(comments_shown).addClass("disabled");
        $("#show-more").removeClass("disabled");
        $("#show-less").addClass("disabled");
    });

    function UpdateCommentsSection() {
        comments_amount = $(".a-comment").length;

        if (comments_amount < 11){
            $("#show-more").addClass("disabled");
        }
        else {
            $("#show-more").removeClass("disabled");
            $(".a-comment").slice(2).addClass("disabled");
        }
    }
});

$('.nav-link').click(function () {
    $(".nav-link").removeClass('active');
    $(this).toggleClass('active');
});
var url = window.location.pathname;
$('nav .nav-link[href="' + url + '"]').toggleClass('active');
function OpenLeftSideMenu() {  
    $(".sidebar").toggleClass('active');
}
// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.