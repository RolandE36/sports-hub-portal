$(function () {
    $("#add-category").click(function (event) {
        $(".hidden-input").remove();
        $("#newItem").val("");
        $(".form-popup h1").html("Add new category");
        $("#newItem").attr("placeholder", "Name your menu category");
        $(".form-popup").css("display", "block");
        $(".background-muted").css("display", "block"); 
    })
    $("#cancelAdding").click(function (event) {
        $("#newItem").val("");
        $(".hidden-input").remove();
        $(".form-popup").css("display", "none");
        $(".background-muted").css("display", "none");
        $('a').prop('disabled', false);
    })
    $("#addCategory").click(function (event) {     
        let count = $(".category").length;
        $(".form-popup").css("display", "none");
        $(".background-muted").css("display", "none");
        $('a').prop('disabled', false);
    })
    $("#add-subcategory").click(function (event) {
        $("#newItem").val("");
        $(".form-popup h1").html("Add new subcategory");
        $("#newItem").attr("placeholder", "Name your menu subcategory");
        $(".form-popup").css("display", "block");
        $(".background-muted").css("display", "block");

    })
})