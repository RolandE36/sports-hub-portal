$(function () {
    var comments_amount;
    var comments_shown = 0;
    UpdateCommentsSection();

    $(".text-input").click(function () {
        $(".submit").removeClass("disabled");
    });

    $("#false-post").click(function () {
        $(".pop-up-overlay.for-login").removeClass("disabled");
    });

    $("#false-like").click(function () {
        $(".pop-up-overlay.for-login").removeClass("disabled");
    });

    $("#false-dislike").click(function () {
        $(".pop-up-overlay.for-login").removeClass("disabled");
    });

    $(".comments-menu").on('click', "#false-comment", function () {
        $(".pop-up-overlay.for-login").removeClass("disabled");
    });

    $(".pop-up-close").click(function () {
        $(".pop-up-overlay.for-login").addClass("disabled");
    });

    $("textarea").each(function () {
        this.setAttribute("style", "height:" + (this.scrollHeight) + "px;overflow-y:hidden;");
    }).on("input", function () {
        this.style.height = "auto";
        this.style.height = (this.scrollHeight) + "px";
    });

    $(".comments-block").on('click', "#show-more", function (event) {
        comments_shown += 10;
        event.preventDefault();
        $(".a-comment").slice(comments_shown).removeClass("disabled");
        $("#show-more").addClass("disabled");
        $("#show-less").removeClass("disabled");
    });

    $(".comments-block").on('click', "#show-less", function (event) {
        comments_shown -= 10;
        event.preventDefault();
        $(".a-comment").slice(comments_shown).addClass("disabled");
        $("#show-more").removeClass("disabled");
        $("#show-less").addClass("disabled");
    });

    function UpdateCommentsSection() {
        comments_amount = $(".a-comment").length;

        if (comments_amount < 11){
            $("#show-more").addClass("disabled");
        }
        else {
            $("#show-more").removeClass("disabled");
            $(".a-comment").slice(2).addClass("disabled");
        }
    }
});
