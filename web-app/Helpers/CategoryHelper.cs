﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text.Json;
using DAL.EF;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using BAL.Interfaces;
using BAL.Managers;
using Microsoft.AspNetCore.Http;

namespace sports_hub_portal.Helpers
{
    public static class CategoryHelper
    {
        public static HtmlString RenderCategories(this IHtmlHelper html)
        {
            HttpContext context = html.ViewContext.HttpContext;
            IServiceProvider services = context.RequestServices;
            ICategoriesManager _categoriesManager = (ICategoriesManager)services.GetService(typeof(ICategoriesManager));
            List<Category> categories = _categoriesManager.GetAllCategories().ToList();

            StringBuilder sb = new StringBuilder();
            
            foreach (var item in categories.Where(c => c.Parent == null))
            {
                var hiddenMark = $"<div class=\"container hidden-mark\" style=\"visibility: hidden\" id=\"hidden-mark-{item.Id}\">hidden</div>";
                if (item.Visibility == false)
                {
                    hiddenMark = $"<div class=\"container hidden-mark\" id=\"hidden-mark-{item.Id}\">hidden</div>";
                }
                sb.AppendLine($"<li class=\"category\" id =\"category-{item.Id}\" onclick =\'showSubcategories({JsonSerializer.Serialize(item.Children?.ToList())},{item.Id})\'" +
                    $"onmouseover=\"ShowActionsDots({item.Id})\" onmouseout=\"HideActionsDots({item.Id})\"" + 
                    ">" +
                        $"{item.Name}" +
                        hiddenMark +
                        $"<div class=\"category-actions\">" +
                            $"<div class=\"category-actions-dots\" id=\"action-dots-{item.Id}\" onmouseover=\"ShowActionsMenu({item.Id})\" onmouseout=\"HideActionsMenu({item.Id})\"></div>" +                       
                            $"<div class=\"container category-actions-popup\" id =\"actions-popup-{item.Id}\" onmouseover=\"ShowActionsMenu({item.Id})\" onmouseout=\"HideActionsMenu({item.Id})\">" +
                                $"<ul class=\"actions-list\">" +
                                    $"<li class=\"actions-list-item\" onclick=\"ChangeVisibility({item.Id}, false)\">Hide</li>" +
                                    $"<li class=\"actions-list-item\" onclick=\"ChangeVisibility({item.Id}, true)\">Show</li>" +
                                $"</ul>" +
                            $"</div>" +
                        $"</div>" +
                    $"</li>");
            }
            return new HtmlString(sb.ToString());
        }
    }
}
