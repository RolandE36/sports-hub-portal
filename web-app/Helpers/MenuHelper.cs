﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL.EF;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using BAL.Interfaces;

namespace sports_hub_portal.Helpers
{
    public static class MenuHelper
    {
        public static HtmlString RenderMenu(this IHtmlHelper html)
        {
            HttpContext context = html.ViewContext.HttpContext;
            IServiceProvider services = context.RequestServices;
            ICategoriesManager _categoriesManager = (ICategoriesManager)services.GetService(typeof(ICategoriesManager));
            List<Category> categories = _categoriesManager.GetAllCategories().ToList();

            StringBuilder sb = new StringBuilder();
            foreach (var item in categories)
            {
                if (item.Parent == null && item.Visibility != false)
                {
                    sb.AppendLine($"<a class=\"nav-link\" href=\"ArticleList\"><img src=\"/img/red-dot.svg\" alt=\"dot\" />{item.Name}</a>");
                }
            }

            return new HtmlString(sb.ToString());
        }
    }
}
