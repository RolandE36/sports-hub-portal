using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using DAL.EF;
using DAL.Models;
using DAL.Services;
using BAL.Interfaces;
using BAL.Managers;
using BAL.Services;

namespace sports_hub_portal
{
	public class Startup
	{
		
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddRazorPages(options =>
			{
				options.Conventions.AuthorizePage("/PersonalPage");
			});
			services.AddAntiforgery(o => o.HeaderName = "XSRF-TOKEN");

            services.AddDbContext<ApplicationContext>();
			services.AddTransient<IUserService, UserService>();
			services.AddIdentity<ApplicationUser, IdentityRole>(setupAction =>
			{
				setupAction.Password.RequireNonAlphanumeric = false;
				setupAction.Password.RequiredLength = 8;
				setupAction.Password.RequireDigit = false;
				setupAction.Password.RequireLowercase = false;
				setupAction.Password.RequireUppercase = false;
				setupAction.User.RequireUniqueEmail = true;

				setupAction.SignIn.RequireConfirmedEmail = true;
				
			})
                .AddEntityFrameworkStores<ApplicationContext>()
				.AddDefaultTokenProviders();

			services.AddAuthentication()
			.AddGoogle(googleOptions =>
			{
				googleOptions.ClientId = Configuration.GetSection("Google").GetSection("ClientId").Value;
				googleOptions.ClientSecret = Configuration.GetSection("Google").GetSection("ClientSecret").Value;
				googleOptions.SignInScheme = IdentityConstants.ExternalScheme;
			})
			.AddFacebook(facebookOptions =>
			{
				facebookOptions.AppId = Configuration.GetSection("Facebook").GetSection("AppId").Value;
				facebookOptions.AppSecret = Configuration.GetSection("Facebook").GetSection("AppSecret").Value;
			});

			services.AddControllersWithViews();

			services.AddTransient<IPersonalInfoManager, PersonalInfoManager>();
			services.AddTransient<ITeamsManager, TeamsManager>();
			services.AddTransient<ICommentsManager, CommentsManager>();
			services.AddTransient<ICategoriesManager, CategoriesManager>();
			services.AddSingleton<ISaveImageManager, SaveImageManager>();
			services.AddTransient<INewsPartnerManager, NewsPartnerManager>();
			services.AddTransient<IArticleManager, ArticleManager>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
		{
			CreateRoles(serviceProvider).Wait();
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Error");
            //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //    app.UseHsts();
            //}

   //         var str = env.EnvironmentName;

            app.UseDeveloperExceptionPage();

			app.UseHttpsRedirection();
			app.UseStaticFiles();
			app.UseRouting();

			app.UseAuthentication();
			app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
		}

		public async Task CreateRoles(IServiceProvider serviceProvider)
        {
			var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
			string[] roles = { "Admin", "User", "SuperAdmin" };

			IdentityResult roleResult;

			foreach (string role in roles)
            {
				var roleExist = await roleManager.RoleExistsAsync(role);

				if (!roleExist)
                {
					roleResult = await roleManager.CreateAsync(new IdentityRole(role));
                }
            }
        }
	}
}