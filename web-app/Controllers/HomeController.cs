﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.EF;
using DAL.Models;
using sports_hub_portal.Views.Categories;
using sports_hub_portal.Pages;

using Microsoft.AspNetCore.Mvc;
namespace sports_hub_portal.Controllers
{

    public class HomeController : Controller
    {
        
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        public IActionResult MoreArticles()
        {
            return View();
        }
        public IActionResult ChangePassword()
        {
            return View();
        }
    }
}
