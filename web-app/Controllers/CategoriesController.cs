﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.EF;
using DAL.Models;
using sports_hub_portal.Views.Categories;
using BAL.Interfaces;


namespace sports_hub_portal.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly ICategoriesManager _categoriesManager;
        public CategoriesController(ICategoriesManager categoriesManager)
        {
            _categoriesManager = categoriesManager;
        }

        public IActionResult Categories()
        {
            var obj = new CategoryModel
            { 
                Categories = _categoriesManager.GetAllCategories().Where(c => c.Parent == null).ToList(),
                Children = _categoriesManager.GetAllCategories().Where(c => c.Parent != null).ToList()
            };            

            return PartialView(obj);
        }
       
        public IActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public IActionResult Add(string categoryName, int? parentId)
        {
            if (!string.IsNullOrEmpty(categoryName))
            {
                foreach (var category in _categoriesManager.GetAllCategories())
                {
                    if(categoryName == category.Name)
                        return RedirectToAction("Categories");
                }
                _categoriesManager.AddCategory(categoryName, parentId);
            }
            return RedirectToAction("Categories");
        }
        [HttpPost]
        public ActionResult ChangeVisibility(int id, bool changeTo)
        {
            try
            {
                _categoriesManager.ChangeVisibility(id, changeTo);
                return Json(new
                {
                    msg = "Successfully " + id
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
