﻿using Microsoft.AspNetCore.Mvc;
using BAL.Managers;
using BAL.Interfaces;
using DAL.Models;
using sports_hub_portal.Views.Shared;

namespace sports_hub_portal.Controllers
{
    public class CommentsController : Controller
    {
        private readonly ICommentsManager _commentsManager;
        public CommentsController(ICommentsManager commentsManager)
        {
            _commentsManager = commentsManager;
        }
        [HttpPost]
        public IActionResult Submit(string userId, string text, string? commentToChangeIdstring, string articleIdstring, string? repliedCommentIdstring)
        {
            int articleId = int.Parse(articleIdstring);
            int? commentToChangeId = null;
            if (commentToChangeIdstring != null)
                commentToChangeId = int.Parse(commentToChangeIdstring);
            int? repliedCommentId = null;
            if (repliedCommentIdstring != null)
                repliedCommentId = int.Parse(repliedCommentIdstring);
            _commentsManager.PostComment(userId, text, commentToChangeId, articleId, repliedCommentId);
            return ViewComponent("Comments", new { articleId = articleId });
        }
        [HttpPost]
        public IActionResult Delete(string commentIdstring, string articleIdstring)
        {
            int commentId = int.Parse(commentIdstring);
            int articleId = int.Parse(articleIdstring);
            _commentsManager.DeleteComment(commentId, articleId);
            return ViewComponent("Comments", new { articleId = articleId });
        }
        [HttpGet]
        public IActionResult RateComment(string commentIdstring, bool isPositive, string userId)
        {
            int commentId = int.Parse(commentIdstring);
            _commentsManager.RateComment(commentId, isPositive, userId);
            return GetRatings(commentId, userId);
        }
        public IActionResult GetRatings(int commentId, string userId)
        {
            var model = new RatingsModel() {commentId = commentId, Ratings = _commentsManager.GetCommentRatings(commentId), userId = userId };
            return PartialView("../Shared/Ratings", model);
        }
        [HttpPost]
        public IActionResult SortComments(string articleIdstring, string sortType)
        {
            int articleId = int.Parse(articleIdstring);
            return ViewComponent("Comments", new { articleId = articleId, sortType = sortType });
        }
    }
}
