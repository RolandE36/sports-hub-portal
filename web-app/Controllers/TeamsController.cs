﻿using BAL.DTOs.Teams;
using BAL.Interfaces;
using BAL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using sports_hub_portal.Views.Teams;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace sports_hub_portal.Controllers
{
    public class TeamsController : Controller
    {
        private readonly ITeamsManager _teamsManager;
        private readonly ISaveImageManager _saveImageManager;
        private readonly ICategoriesManager _categoriesManager;

        private string GetMapToken() 
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            return builder.Build().GetSection("MapToken").Value;
        }
        private TeamsTableModel GetDefaultTableModel() //gets default teams table model
        {
            var teamsInfo = _teamsManager.GetTeams("", 0, 0, 0, 0, 5, null, "descending");
            int pageQuantity = teamsInfo.QuantityOfResults / 5;
            if (teamsInfo.QuantityOfResults % 5 > 0)
                pageQuantity++;
            return new TeamsTableModel { Teams = teamsInfo, QuantityOfPages = pageQuantity, CurrentPage = 1, QuantityOfResultsOnPage = 5 };
        }
        public TeamsController(ITeamsManager teamsManager, ISaveImageManager saveImageManager, ICategoriesManager categoriesManager) 
        {
            _teamsManager = teamsManager;
            _saveImageManager = saveImageManager;
            _categoriesManager = categoriesManager;
        }
        public IActionResult TeamsPage()
        {
            TeamsPageModel model = new TeamsPageModel();
            model.Categories = _categoriesManager.GetCategories();
            model.TeamLocations = _teamsManager.GetTeamLocations();
            model.TableModel = GetDefaultTableModel();
            model.Token = GetMapToken();
            model.ResultState = false;
            return View(model);
        }
        [HttpGet]
        public IActionResult GetSubcategories(int CategoryId) 
        {
            return new JsonResult(_categoriesManager.GetSubcategoriesOfCategory(CategoryId));
        }
        [HttpPost]
        public IActionResult PostTeamLogo([FromForm] IFormFile TeamLogo) 
        {
            string result = _saveImageManager.SaveImage(TeamLogo, "TeamLogoUploads");
            if (result is null)
            {
                result = "";
                Response.StatusCode = 500;
            }
            return Content(result);
        }
        [HttpPost]
        public IActionResult AddTeam([FromForm] int TeamLocation, [FromForm] int TeamCategory, [FromForm] int TeamSubcategory, [FromForm] string TeamName, [FromForm] string TeamLogo, [FromForm] int EditingId) 
        {
            TeamsPageModel model = new TeamsPageModel();
            model.Categories = _categoriesManager.GetCategories();
            model.TeamLocations = _teamsManager.GetTeamLocations();
            try
            {
                if (String.IsNullOrEmpty(TeamName))
                {
                    model.TableModel = GetDefaultTableModel();
                    model.Token = GetMapToken();
                    model.Result = "Invalid team name.";
                    model.ResultState = false;
                    return View("TeamsPage", model);
                }
                if (String.IsNullOrEmpty(TeamLogo))
                {
                    model.TableModel = GetDefaultTableModel();
                    model.Token = GetMapToken();
                    model.Result = "Please add team logo.";
                    model.ResultState = false;
                    return View("TeamsPage", model);
                }
                if (TeamLocation < 1)
                {
                    model.TableModel = GetDefaultTableModel();
                    model.Token = GetMapToken();
                    model.Result = "Invalid location Id.";
                    model.ResultState = false;
                    return View("TeamsPage", model);
                }
                if (TeamSubcategory < 1)
                {
                    if (TeamCategory < 1) {
                        model.TableModel = GetDefaultTableModel();
                        model.Token = GetMapToken();
                        model.Result = "Invalid team subcategory Id.";
                        model.ResultState = false;
                        return View("TeamsPage", model);
                    }
                }
                if (EditingId>0) 
                {
                    if (_teamsManager.EditTeam(EditingId, TeamLocation, TeamCategory, TeamSubcategory, TeamName, TeamLogo))
                    {
                        model.TableModel = GetDefaultTableModel();
                        model.Token = GetMapToken();
                        model.Result = "Successfully updated team.";
                        model.ResultState = true;
                        return View("TeamsPage", model);
                    }
                    else {
                        model.TableModel = GetDefaultTableModel();
                        model.Token = GetMapToken();
                        model.Result = "Unable to update team info, try again later.";
                        model.ResultState = false;
                        return View("TeamsPage", model);
                    }
                }
                else if (_teamsManager.AddTeam(TeamLocation, TeamCategory, TeamSubcategory, TeamName, TeamLogo))
                {
                    model.TableModel = GetDefaultTableModel();
                    model.Token = GetMapToken();
                    model.Result = "Successfully added team";
                    model.ResultState = true;
                    return View("TeamsPage", model);
                }
                else {
                    model.TableModel = GetDefaultTableModel();
                    model.Token = GetMapToken();
                    model.Result = "Unable to add team, try again later.";
                    model.ResultState = false;
                    return View("TeamsPage", model);
                }
            }
            catch(Exception)
            {
                model.Result = "Uknown error";
                model.ResultState = false;
                model.TableModel = GetDefaultTableModel();
                model.Token = GetMapToken();
                return View("TeamsPage", model);
            }
        }
        [HttpPost]
        public IActionResult DeleteTeam([FromForm]int Id) 
        {
            TeamsPageModel model = new TeamsPageModel();
            model.Categories = _categoriesManager.GetCategories();
            model.Token = GetMapToken();
            model.TeamLocations = _teamsManager.GetTeamLocations();
            if (Id < 1) {
                model.TableModel = GetDefaultTableModel();
                model.Result = "Specify right id";
                model.ResultState = false;
                return View("TeamsPage", model);
            }
            if (_teamsManager.DeleteTeam(Id))
            {
                model.TableModel = GetDefaultTableModel();
                model.Result = "Successfully deleted team.";
                model.ResultState = true;
                return View("TeamsPage", model);
            }
            else {
                model.Result = "Uknown error";
                model.ResultState = false;
            }
            model.TableModel = GetDefaultTableModel();
            return View("TeamsPage", model);
        }
        [HttpGet]
        public IActionResult GetTeamsTable(string Name, int LocationId, int CategoryId, int SubcategoryId, int Page, int QuantityOfRows, string SortBy, string SortType) 
        {
            if (Page < 1) Page = 1;
            if (QuantityOfRows < 3) QuantityOfRows = 3;
            var teamsInfo = _teamsManager.GetTeams(Name, LocationId, CategoryId, SubcategoryId, QuantityOfRows * (Page - 1), QuantityOfRows, SortBy, SortType);
            int pageQuantity = teamsInfo.QuantityOfResults / QuantityOfRows;
            if (teamsInfo.QuantityOfResults % QuantityOfRows > 0)
                pageQuantity++;
            var model = new TeamsTableModel { Teams = teamsInfo, QuantityOfPages = pageQuantity, CurrentPage = Page, QuantityOfResultsOnPage = QuantityOfRows };
            return PartialView("TeamsTable", model);
        }
        [HttpPost]
        public IActionResult AddLocation([FromForm]string LocationName, [FromForm]double Longitude, [FromForm]double Latitude) 
        {
            TeamsPageModel model = new TeamsPageModel();
            model.Categories = _categoriesManager.GetCategories();
            model.TableModel = GetDefaultTableModel();
            model.Token = GetMapToken();
            if (string.IsNullOrEmpty(LocationName))
            {
                model.TeamLocations = _teamsManager.GetTeamLocations();
                model.Result = "Please specify location name";
                model.ResultState = false;
                return View("TeamsPage", model);
            }
            if (_teamsManager.AddTeamLocation(LocationName, Longitude, Latitude)) 
            {
                model.TeamLocations = _teamsManager.GetTeamLocations();
                model.Result = "Successfully added location";
                model.ResultState = true;
                return View("TeamsPage", model);
            }
            model.TeamLocations = _teamsManager.GetTeamLocations();
            model.Result = "Unable to add location";
            model.ResultState = false;
            return View("TeamsPage", model);
        }
    }
}
