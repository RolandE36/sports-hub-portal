﻿using BAL.DTOs.Teams;
using BAL.Interfaces;
using BAL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using sports_hub_portal.Views.News;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace sports_hub_portal.Controllers
{
    public class NewsController : Controller
    {
        private readonly INewsPartnerManager _newsPartnerManager;
        private readonly ICategoriesManager _categoriesManager;
        public NewsController(INewsPartnerManager newsPartnerManager, ICategoriesManager categoriesManager) 
        {
            _newsPartnerManager = newsPartnerManager;
            _categoriesManager = categoriesManager;
        }
        public IActionResult NewsPartners() 
        {
            var model = new NewsPartnersModel();
            model.NewsPartners = _newsPartnerManager.GetNewsPartners();
            model.Categories = _categoriesManager.GetCategories();
            model.ResultState = false;
            return View(model);
        }
        [HttpPost]
        public IActionResult AddPartner([FromForm] string NewsProvider)
        {
            var model = new NewsPartnersModel();
            if (string.IsNullOrEmpty(NewsProvider))
            {
                model.Result = "Missing provider name";
                model.ResultState = false;
                model.NewsPartners = _newsPartnerManager.GetNewsPartners();
                model.Categories = _categoriesManager.GetCategories();
                return View("NewsPartners",model);
            }
            if (_newsPartnerManager.AddNewsPartner(NewsProvider)) 
            {
                model.Result = "Successfully added new partner.";
                model.ResultState = true;
                model.NewsPartners = _newsPartnerManager.GetNewsPartners();
                model.Categories = _categoriesManager.GetCategories();
                return View("NewsPartners",model);
            }
            model.Result = "Uknown error.";
            model.ResultState = false;
            model.Categories = _categoriesManager.GetCategories();
            model.NewsPartners = _newsPartnerManager.GetNewsPartners();
            return View("NewsPartners",model);
        }
        [HttpPost]
        public IActionResult EditPartner([FromForm]int Id, [FromForm]string ApiKey, [FromForm]string Sources) 
        {
            try
            {
                var model = new NewsPartnersModel();
                model.Categories = _categoriesManager.GetCategories();
                if (Id < 1) {
                    model.Result = "Invalid Id";
                    model.ResultState = false;
                    model.NewsPartners = _newsPartnerManager.GetNewsPartners();
                    return View("NewsPartners", model);
                }
                List<int> enabledCategories = new List<int>();
                Dictionary<int, string> categorySources = new Dictionary<int, string>();
                foreach (var key in this.HttpContext.Request.Form.Keys)
                {
                    if (key.Contains("CheckCategory"))
                    {
                        enabledCategories.Add(Convert.ToInt32(key.Split('-')[1]));
                    }
                    else if (key.Contains("CategorySources"))
                    {
                        StringValues value;
                        if (!this.HttpContext.Request.Form.TryGetValue(key, out value)) {
                            model.Result = "Uknown error";
                            model.ResultState = false;
                            model.NewsPartners = _newsPartnerManager.GetNewsPartners();
                            return View("NewsPartners", model);
                        }
                        categorySources.Add(Convert.ToInt32(key.Split('-')[1]), value.ToString());
                    }
                }
                if (!_newsPartnerManager.EditPartner(Id, ApiKey, Sources, enabledCategories, categorySources)) {
                    model.Result = "Uknown error";
                    model.ResultState = false;
                    model.NewsPartners = _newsPartnerManager.GetNewsPartners();
                    return View("NewsPartners", model);
                }
                model.Result = "Successfully updated partner info";
                model.ResultState = true;
                model.NewsPartners = _newsPartnerManager.GetNewsPartners();
                return View("NewsPartners", model);
            }
            catch(Exception e)
            {
                var model = new NewsPartnersModel();
                model.Result = "Uknown error";
                model.ResultState = false;
                model.Categories = _categoriesManager.GetCategories();
                model.NewsPartners = _newsPartnerManager.GetNewsPartners();
                return View("NewsPartners", model);
                throw new NotImplementedException();
            }
        }
        public class ToggleObjectInfo
        {
            public string id { get; set; }
            public bool state { get; set; }
        }
        [HttpPost]
        public IActionResult TogglePartner([FromBody]ToggleObjectInfo info) 
        {
            if (!_newsPartnerManager.TogglePartner(Convert.ToInt32(info.id), info.state))
                return StatusCode(StatusCodes.Status500InternalServerError);
            return Ok();
        }
        [HttpPost]
        public IActionResult DeletePartner([FromForm]int PartnerId) 
        {
            var model = new NewsPartnersModel();
            if (PartnerId < 1) 
            {
                model.Result = "Specify valid partner id.";
                model.ResultState = false;
                model.Categories = _categoriesManager.GetCategories();
                model.NewsPartners = _newsPartnerManager.GetNewsPartners();
                return View("NewsPartners", model);
            }
            if (_newsPartnerManager.DeletePartner(PartnerId))
            {
                model.Result = "Partner was successfully deleted.";
                model.ResultState = true;
                model.Categories = _categoriesManager.GetCategories();
                model.NewsPartners = _newsPartnerManager.GetNewsPartners();
                return View("NewsPartners", model);
            }
            else {
                model.Result = "Uknown error";
                model.ResultState = false;
                model.Categories = _categoriesManager.GetCategories();
                model.NewsPartners = _newsPartnerManager.GetNewsPartners();
                return View("NewsPartners", model);
            }
        }
    }
}
