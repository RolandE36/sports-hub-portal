﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using DAL.Models;
using sports_hub_portal.Views.Account;
using Microsoft.AspNetCore.Authorization;
using DAL.Services;
using System.Linq;
using DAL.EF;
using System.Security.Claims;
using BAL.Interfaces;

namespace sports_hub_portal.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationContext _applicationContext;
        private readonly IUserService _userService;
        private readonly IPersonalInfoManager _personalInfoManager;
        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager , ApplicationContext applicationContext, IUserService userService, IPersonalInfoManager personalInfoManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _applicationContext = applicationContext;
            _userService = userService;
            _personalInfoManager = personalInfoManager;
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null, string resetPassword = null)
        {
            ViewData["ResetPasswordText"] = resetPassword;
            return View(new LoginModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    if (!await _userManager.IsEmailConfirmedAsync(user))
                    {
                        ModelState.AddModelError("Email", "Email wasn't confirmed");
                        return View(model);
                    }
                }
                else
                {
                    ModelState.AddModelError("Password", "There is no user with this email");
                    return View(model);
                }

                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, true, false);

                if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("Password", "Incorrect login and (or) password");
                }
            }
            return View();

        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register() => View();

        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel model)
        {

            if (ModelState.IsValid)
            {
                if (model.FirstName.Any(char.IsDigit) || model.LastName.Any(char.IsDigit))
                {
                    ModelState.AddModelError("Password", "FisrtName or LastName can't contains numbers");
                    return View(model);
                }


                ApplicationUser user = new ApplicationUser
                {
                    Email = model.Email,
                    UserName = model.Email,
                    UserProfile = new UserProfile
                    {
                        EmailAddess = model.Email,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        AvatarName = "default-avatar.png"
                    }
            };

                var result = await _userManager.CreateAsync(user, model.Password);
                result = await _userManager.AddToRoleAsync(user, "User");

                if (result.Succeeded)
                {
                    var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var link = Url.Action(nameof(VerifyEmail), "Account", new { userId = user.Id, token }, protocol: HttpContext.Request.Scheme);

                    await SendEmail(model.Email, "Confirm your account", link, "./wwwroot/html/ConfirmationLetter.html");

                    return RedirectToAction("SendLetterToEmail", new { email = model.Email });
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("Password", error.Description);
                    }
                }
            }
            return View(model);
        }


        [Route("/Account/GoogleFacebook/{provider}/{task}")]
        public IActionResult GoogleFacebook(string provider, string task)
        {
            string redirectUrl = Url.Action(task, "Account");
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return new ChallengeResult(provider, properties);
        }

        [AllowAnonymous]
        public async Task<IActionResult> GoogleFacebookLoginResponse()
        {
            ExternalLoginInfo info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
                return RedirectToAction(nameof(Login));

            var email = info.Principal.FindFirst(ClaimTypes.Email).Value;

            var user = await _userManager.FindByEmailAsync(email);

            if (user == null) return RedirectToAction("AccessDenied", new { message = "There is no user registered with this email" });
            else
            {
                await _signInManager.SignInAsync(user, false);
                return RedirectToAction("Index", "Home");
            }
        }

        public async Task<IActionResult> GoogleFacebookRegisterResponse()
        {
            ExternalLoginInfo info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
                return RedirectToAction(nameof(Login));


            var userNames = info.Principal.Identity.Name.Split(" ");
            ApplicationUser user = new ApplicationUser
            {
                Email = info.Principal.FindFirst(ClaimTypes.Email).Value,
                UserName = info.Principal.FindFirst(ClaimTypes.Email).Value,
                EmailConfirmed = true,
                UserProfile = new UserProfile
                {
                    EmailAddess = info.Principal.FindFirst(ClaimTypes.Email).Value,
                    FirstName = userNames[0],
                    LastName = userNames[1],
                    AvatarName = "default-avatar.png"
                }
            };

            IdentityResult identResult = await _userManager.CreateAsync(user);
            if (identResult.Succeeded)
            {
                identResult = await _userManager.AddLoginAsync(user, info);
                identResult = await _userManager.AddToRoleAsync(user, "User");
                if (identResult.Succeeded)
                {
                    await SendEmail(user.Email);
                    await _signInManager.SignInAsync(user, false);
                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("AccessDenied", new { message = "Maybe you already have account with this email" });
        }

        [HttpGet]
        public IActionResult ForgotPassword() => View();

        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                ModelState.AddModelError("Email", "There is no user with this email");
                return View();
            }

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var link = Url.Action(nameof(ResetPassword), "Account", new { userId = user.Id, token }, protocol: HttpContext.Request.Scheme);

            await SendEmail(model.Email, "Reset Your InVision Password", link, "./wwwroot/html/ResetPassword.html");

            return RedirectToAction("SendLetterToEmail", new { email = model.Email });
        }

        [HttpGet]
        public IActionResult ResetPassword(string userId, string token)
        {
            var model = new ResetPasswordModel { UserId = userId, Token = token };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model)
        {
            var user = await _userManager.FindByIdAsync(model.UserId);
            if (ModelState.IsValid)
            {
                var result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);
                if (result.Succeeded)
                    return RedirectToAction("Login", new { resetPassword = "Your password has been updated" });
                else
                {
                    foreach (var error in result.Errors)
                        ModelState.AddModelError("ConfirmPassword", error.Description);
                }
            }
            return View();
        }
        public IActionResult AccessDenied(string message)
        {
            ViewData["Message"] = message;
            return View();
        }

        public IActionResult SuccessfullRegistration(string account)
        {
            ViewData["Account"] = account;
            return View();
        }

        public IActionResult SendLetterToEmail(string email)
        {
            ViewData["Email"] = email;
            return View();
        }

        public async Task<IActionResult> VerifyEmail(string userId, string token)
        {
            if (userId == null || token == null)
            {
                return View("Error");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return View("Error");
            }
            var result = await _userManager.ConfirmEmailAsync(user, token);
            if (result.Succeeded)
                return RedirectToAction("ConfirmEmail");
            else
                return View("Error");
        }

        public IActionResult ConfirmEmail() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        public async Task SendEmail(string email)
        {
            EmailService emailService = new EmailService();
            string confirmationLetter = System.IO.File.ReadAllText("./wwwroot/html/SuccessRegistration.html");

            await emailService.SendEmailAsync(email, "Success Registration", confirmationLetter);
        }

        public async Task SendEmail(string email, string subjeсt, string link, string path)
        {
            EmailService emailService = new EmailService();
            string confirmationLetter = System.IO.File.ReadAllText(path).Replace("LINK", link);

            await emailService.SendEmailAsync(email, subjeсt, confirmationLetter);
        }

        [AllowAnonymous]
        public async Task<IActionResult> ChangeEmail(string userId, string token)
        {
            var result = await _personalInfoManager.ChangeEmailAsync(userId, token);
            if(result.Succeeded) 
                return RedirectToAction("ConfirmEmail");
            else
                return View("Error");
        }

        public async Task<IdentityResult> ChangePasswordAsync(ChangePasswordModel model)
        {
            var userId = _userService.GetUserId();
            var user = await _userManager.FindByIdAsync(userId);
            return await _userManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);
        }
        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel model)
        {

            var passwordValidator = new PasswordValidator<ApplicationUser>();
            if (ModelState.IsValid)
            {
                
                var validPass = await passwordValidator.ValidateAsync(_userManager, null, model.CurrentPassword);
                if (!validPass.Succeeded)
                {
                    ModelState.AddModelError("CurrentPassword", "Incorrect password");
                    return View(model);
                }
                if (!model.NewPassword.Any(char.IsUpper))
                {
                    ModelState.AddModelError("NewPassword", "Entred password format is wrong!");
                    return View(model);
                }
                if (!model.NewPassword.Any(char.IsDigit))
                {
                    ModelState.AddModelError("NewPassword", "Entred password format is wrong!");
                    return View(model);
                }

                else
                {
                    var result = await ChangePasswordAsync(model);
                    if (result.Succeeded)
                    {
                        ViewBag.IsSuccess = true;
                        ModelState.Clear();
                        return View();
                    }

                    foreach (var error in result.Errors)
                    {
                        
                        ModelState.AddModelError("", error.Description);
                    }
                }
            }
            return View(model);
        }
    }
}

