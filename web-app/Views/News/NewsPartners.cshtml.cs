﻿using BAL.DTOs;
using BAL.DTOs.Category;
using BAL.DTOs.Partner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sports_hub_portal.Views.News
{
    public class NewsPartnersModel
    {
        public List<NewsPartnerDTO> NewsPartners { get; set; }
        public List<CategoryDTO> Categories { get; set; }
        public bool ResultState { get; set; }
        public string Result { get; set; }
    }
}
