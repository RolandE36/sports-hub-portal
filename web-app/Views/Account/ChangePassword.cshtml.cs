using System.ComponentModel.DataAnnotations;

namespace sports_hub_portal.Views.Account
{
    public class ChangePasswordModel
    {
        
        [Required, DataType(DataType.Password), Display(Name = "Current password")] 
        public string CurrentPassword { get; set; }
        [Required, DataType(DataType.Password), Display(Name = "New password")]
        [StringLength(100, ErrorMessage = "Entred password format is worng", MinimumLength = 8)]
        public string NewPassword { get; set; }
        [Required, DataType(DataType.Password), Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "Confirm new password does not match")]
        public string ConfirmPassword { get; set; }
        public string ReturnUrl { get; set; }

    }
}
