using System.ComponentModel.DataAnnotations;

namespace sports_hub_portal.Views.Account
{
    public class ForgotPasswordModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
