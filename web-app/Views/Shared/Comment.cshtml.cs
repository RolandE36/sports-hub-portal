using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using DAL.EF;

namespace sports_hub_portal.Views.Shared
{
	public class CommentModel : PageModel
	{
		public Comment comment;
		public List<Comment> replies;
		public UserProfile currentUser;
	}
}
