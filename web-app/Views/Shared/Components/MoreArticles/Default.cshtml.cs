using DAL.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;

namespace sports_hub_portal.Views.Shared.Components.MoreArticles
{
	public class MoreArticlesModel : PageModel
	{
		public List<Article> Articles { get; set; }
	}
}
