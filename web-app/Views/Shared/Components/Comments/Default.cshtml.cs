﻿using DAL.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;
using DAL.EF;

namespace sports_hub_portal.Views.Shared.Components.Comments
{
	public class SectionModel : PageModel
	{
		public UserProfile currentUser { get; set; }
		public List<Comment> comments { get; set; }
		public List<Comment> replies { get; set; }
		public int articleId { get; set; }
		public string sortType { get; set; }
	}
}
