using DAL.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;

namespace sports_hub_portal.Views.Shared
{
	public class RatingsModel : PageModel
	{
		public int commentId { get; set; }
		public List<CommentRating> Ratings { get; set; }
		public string userId { get; set; }
	}
}
