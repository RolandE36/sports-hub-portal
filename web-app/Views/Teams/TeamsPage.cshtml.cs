﻿using BAL.DTOs;
using BAL.DTOs.Category;
using BAL.DTOs.Teams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sports_hub_portal.Views.Teams
{
    public class TeamsPageModel
    {
        public string Token { get; set; }  
        public List<CategoryDTO> Categories { get; set; }
        public bool ResultState { get; set; }
        public string Result { get; set; }
        public TeamsTableModel TableModel { get; set; }
        public List<TeamLocationsDTO> TeamLocations { get; set; }
    }
}
