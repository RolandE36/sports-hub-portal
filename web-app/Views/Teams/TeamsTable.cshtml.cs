﻿using BAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sports_hub_portal.Views.Teams
{
    public class TeamsTableModel
    {
        public TeamsInfoDTO Teams { get; set; }
        public int QuantityOfPages { get; set; }
        public int CurrentPage { get; set; }
        public int QuantityOfResultsOnPage { get; set; }
    }
}
