using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using DAL.Models;

namespace sports_hub_portal.Views.Categories
{
    public class CategoryModel : PageModel
    {

        public List<Category> Categories { get; set; }
        public List<Category> Children { get; set; }
        
        public void OnGet()
        {
        }
    }
}