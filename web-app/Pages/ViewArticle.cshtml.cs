using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc.RazorPages;

using DAL.Models;

using BAL.Interfaces;

namespace sports_hub_portal.Pages
{
    public class ViewArticleModel : PageModel
    {
        private readonly IArticleManager articleManager;
        public Article articles;

        public ViewArticleModel(IArticleManager manager)
        {
            articleManager = manager;
        }
        public void OnGet(int id)
        {
            articles = articleManager.GetArticle(id);
        }
    }
}
