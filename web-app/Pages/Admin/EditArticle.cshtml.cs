using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DAL;
using DAL.EF;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace sports_hub_portal.Pages.Admin
{
    [Authorize]
    public class EditArticleModel : PageModel
    {
        ApplicationContext _context;
        ILogger<EditArticleModel> _logger;
        public EditArticleModel(ILogger<EditArticleModel> logger, ApplicationContext db)
        {
            _logger = logger;
            _context = db;
        }
        [BindProperty]
        public Article Article { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Conference> Conferences { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Team> Teams;
        public void OnGet(int id)
        {
            
            Locations =  _context.Locations;
            Categories = _context.Categories;
            Conferences =_context.Conferences;
            Article = _context.Articles.Find(id);
            Article.Image = _context.Images.Find(Article.ImageId);
            Teams = _context.Teams;
        }
        public IActionResult OnPost(int id, string? photoname)
        {
            var article = _context.Articles.Find(id);
            
            article.LocationId = Article.LocationId;
            article.ConferenceId = Article.ConferenceId;
            article.CategoryId = Article.CategoryId;
            article.Caption = Article.Caption;
            article.Alt = Article.Alt;
            article.Content = Article.Content;
            article.Headline = Article.Headline;
            article.Public = Article.Public;
            article.Commentable = Article.Commentable;
            if(photoname != null)
            {
                var file = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "img", "ArticleImages", photoname);
                if (System.IO.File.Exists(file))
                {
                    var img = new Image { Url = photoname };
                    _context.Images.Add(img);
                    _context.SaveChanges();
                    article.ImageId = img.Id;
                }
            }
            
            _context.SaveChanges();
            return Redirect("./Articles");
        }
        
        public IFormFile ArticleImage { get; set; }
        public IActionResult OnPostImageUploadHandlerForArticle()
        {
            string PhotoName = Guid.NewGuid().ToString() + ArticleImage.FileName.Substring(ArticleImage.FileName.LastIndexOf("."));
            var file = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "img", "ArticleImages", PhotoName);
            using (var fileStream = new FileStream(file, FileMode.Create))
            {
                ArticleImage.CopyTo(fileStream);
            }
            return Content(PhotoName);
        }
    }
}
