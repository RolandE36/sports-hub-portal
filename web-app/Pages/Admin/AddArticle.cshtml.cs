using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL.EF;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace sports_hub_portal.Pages.Admin
{
    [Authorize]
    public class AddArticleModel : PageModel
    {
        ILogger<AddArticleModel> _logger;
        ApplicationContext _context;
        [BindProperty]
        public Article Article { get; set; }
        
        public DbSet<Category> Categories { get; set; }

        public DbSet<Team> Teams { get; set; }
        public DbSet<Conference> Conferences { get; set; }
        
        public IEnumerable<Location> Locations { get; set; }
        public AddArticleModel(ILogger<AddArticleModel> logger,ApplicationContext db)
        {
            
            _context = db;
            _logger = logger;
        }
        public void OnGet(int? categoryId)
        {
            if(categoryId == null)
            {
                Conferences = _context.Conferences;
                Locations = _context.Locations;
                Teams = _context.Teams;
            }
            else
            {
                
            }
        }
        public IActionResult OnPost(string photoname)
        {
            Categories = _context.Categories;
            Conferences = _context.Conferences;
            Locations = _context.Locations;
            Teams = _context.Teams;
            if (ModelState.IsValid)
            {
                Article.Location = Locations.First(l => l.Id == Article.LocationId);
                Article.Conference = Conferences.First(l => l.Id == Article.ConferenceId);
                Article.Category = Categories.First(l => l.Id == Article.CategoryId);
                Article.Team = Teams.First(l => l.Id == Article.TeamId);
                var file = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "img", "ArticleImages",photoname);
                if (System.IO.File.Exists(file))
                {
                    var img = new Image { Url = photoname };
                    _context.Images.Add(img);
                    _context.SaveChanges();
                    Article.ImageId = img.Id;
                }
                _context.Articles.Add(Article);
                _context.SaveChanges();
            }
            
            return RedirectToPage("./Articles");
        }
        // added handler
        public IFormFile ArticleImage { get; set; }
        public IActionResult OnPostImageUploadHandlerForArticle() 
        {
            string PhotoName = Guid.NewGuid().ToString() + ArticleImage.FileName.Substring(ArticleImage.FileName.LastIndexOf("."));
            var file = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "img", "ArticleImages", PhotoName);
            using (var fileStream = new FileStream(file, FileMode.Create))
            {
                ArticleImage.CopyTo(fileStream);
            }
            return Content(PhotoName);
        }
        //
    }
}
