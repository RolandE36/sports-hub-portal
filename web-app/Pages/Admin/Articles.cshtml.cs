using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using DAL.EF;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;

namespace sports_hub_portal.Pages.Admin
{
    [Authorize]
    public class ArticleModel : PageModel
    {
        private readonly ILogger<ArticleModel> _logger;
        private readonly ApplicationContext _context;
        public IEnumerable<Article> Articles;
        public DbSet<Conference> Conferences;
        public DbSet<Category> Categories;
        public DbSet<Team> Teams;
        
        public ArticleModel(ILogger<ArticleModel> logger, ApplicationContext db)
        {
            _context = db;
            _logger = logger;
            Articles = _context.Articles;
        }
        public void OnGet(string type)
        {
            if (type == null)
            {
                Articles = _context.Articles.Include(a => a.Conference)
                .Include(a => a.Team)
                .Include(a => a.Location)
                .Include(a => a.Category)
                .Include(a => a.Image)
                .ToList();
            }
            else
            {
                Articles = _context.Articles.Include(a => a.Conference)
                .Include(a => a.Team)
                .Include(a => a.Location)
                .Include(a => a.Category)
                .Include(a => a.Image)
                .ToList().FindAll(x => x.Conference.Name.Contains(type));
                if (Articles.Count() == 0)
                {
                    Articles = _context.Articles.Include(a => a.Conference)
                .Include(a => a.Team)
                .Include(a => a.Location)
                .Include(a => a.Category)
                .Include(a => a.Image)
                .ToList().FindAll(x => x.Team.Name.Contains(type));
                }
                if (Articles.Count() == 0)
                {
                    Articles = _context.Articles.Include(a => a.Conference)
                .Include(a => a.Team)
                .Include(a => a.Location)
                .Include(a => a.Category)
                .Include(a => a.Image)
                .ToList().FindAll(x => x.Public == true && type == "1" ? true : false);
                }
                if (Articles.Count() == 0)
                {
                    Articles = _context.Articles.Include(a => a.Conference)
                .Include(a => a.Team)
                .Include(a => a.Location)
                .Include(a => a.Category)
                .Include(a => a.Image)
                .ToList().FindAll(x => x.Public == false && type == "0" ? true : false);
                }
            }
            Categories = _context.Categories;
            Conferences = _context.Conferences;
            Teams = _context.Teams;
        }
        public IActionResult OnGetUpdate()
        {
            return RedirectToPage();
        }
        public IActionResult OnPostDelete(int id)
        {
            Article article;
            if ((article = _context.Articles.Find(id)) != null)
            {
                _context.Articles.Remove(article);
                _context.SaveChanges();
            }
            else
            {
                _logger.LogError("Delete: No content result");
                return new NoContentResult();
            }
            return RedirectToPage();
        }
        public IActionResult OnPostPublish(int id)
        {
            _context.Articles.Find(id).Public = true;
            _context.SaveChanges();
            return RedirectToPage();
        }
        public IActionResult OnPostUnpublish(int id)
        {
            _context.Articles.Find(id).Public = false;
            _context.SaveChanges();
            return RedirectToPage();
        }
        public IActionResult OnPostTurnOnComments(int id)
        {
            _context.Articles.Find(id).Commentable = true;
            _context.SaveChanges();
            return RedirectToPage();
        }
        public IActionResult OnPostTurnOffComments(int id)
        {
            _context.Articles.Find(id).Commentable = false;
            _context.SaveChanges();
            return RedirectToPage();
        }
        public IActionResult OnPostMoveByCategory(int articleId, int categoryId)
        {
            if (_context.Categories.First(c => c.Id == categoryId) != null)
            {
                
                _context.Articles.Find(articleId).CategoryId = categoryId;
                _context.SaveChanges();
            }
            else
            {
                _logger.LogError($"PostMoveCategory: category with id {categoryId} doesn't exist");
            }
            return RedirectToPage();
        }
    }   
}
