﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models;
using DAL.EF;

namespace sports_hub_portal.Pages
{
	public class IndexModel : PageModel
	{
		private readonly ILogger<IndexModel> _logger;
		public IndexModel(ILogger<IndexModel> logger)
		{
			_logger = logger;
			
		}
		
		public void OnGet()
		{

		}
	}
}
