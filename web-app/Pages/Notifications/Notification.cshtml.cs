﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sports_hub_portal.Pages.Notifications
{
    public class NotificationModel
    {
        public bool Sucsess { get; set; }
        public bool EnableRetryButton { get; set; }
        public bool InitiallyHidden { get; set; }
        public string Id { get; set; }
        public string Header { get; set; }
        public string MainText { get; set; }
    }
}
