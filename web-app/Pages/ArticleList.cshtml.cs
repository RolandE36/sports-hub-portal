using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc.RazorPages;

using DAL.Models;

using BAL.Interfaces;

namespace sports_hub_portal.Pages
{
    public class ArticleListModel : PageModel
    {
        private readonly IArticleManager articleManager;
        public List<Article> articles;

        public ArticleListModel(IArticleManager manager)
        {
            articleManager = manager;
        }

        public void OnGet()
        {
            articles = articleManager.GetArticleList();
        }
    }
}