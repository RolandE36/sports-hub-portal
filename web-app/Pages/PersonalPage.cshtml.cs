using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Identity;
using DAL.Models;
using DAL.EF;
using DAL.Services;
using BAL.Interfaces;
using BAL.Services;

namespace sports_hub_portal.Pages
{
    public class UploadModel : PageModel
    {
        private readonly IPersonalInfoManager _personalInfoManager;
        private readonly ISaveImageManager _saveImageService;

        public IFormFile Avatar { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AvatarName { get; set; }
        public bool ShowNotification { get; set; } = false;
        public bool UpdateResultSuccess { get; set; }
        public bool UpdatedEmail { get; set; }
        public bool EmailExists { get; set; }

        public UploadModel(IPersonalInfoManager personalInfoManager, ISaveImageManager saveImageService) 
        {
            _personalInfoManager = personalInfoManager;
            _saveImageService = saveImageService;
        }
        public async Task OnGet() 
        {
            var userProfile = await _personalInfoManager.GetPersonalAsync(User);
            Email = userProfile.Email;
            FirstName = userProfile.FirstName;
            LastName = userProfile.LastName;
            AvatarName = userProfile.AvatarName;
        }
        public IActionResult OnPostImageUploadHandler() 
        {
            string result = _saveImageService.SaveImage(Avatar, "AvatarUploads");
            if (result is null) 
            {
                result = "";
                Response.StatusCode = 500;
            }
            return Content(result);
        }
        public async Task OnPostUserInfoHandler() 
        {
            ShowNotification = true;
            EmailExists = false;
            string avatarName = string.Empty;
            string firstName = string.Empty;
            string lastName = string.Empty;
            string email = string.Empty;
            try
            {
                avatarName = Request.Form["NewAvatarName"];
                firstName = Request.Form["FirstName"];
                lastName = Request.Form["LastName"];
                email = Request.Form["Email"];
                var userProfile = await _personalInfoManager.GetPersonalAsync(User);
                Email = userProfile.Email;
                FirstName = userProfile.FirstName;
                LastName = userProfile.LastName;
                AvatarName = userProfile.AvatarName;
                if (userProfile.Email != email)
                {
                    var tokenResult =await _personalInfoManager.GenerateEmailChangeVerificationTokenAsync(User, email);
                    if(tokenResult.Token is null)
                        throw new Exception();
                    var link = Url.Action("ChangeEmail", "Account", new { tokenResult.UserId, tokenResult.Token}, protocol: HttpContext.Request.Scheme);
                    EmailService emailService = new EmailService();
                    string confirmationLetter = System.IO.File.ReadAllText("./wwwroot/html/ConfirmationLetter.html").Replace("LINK", link);
                    await emailService.SendEmailAsync(email, "Confirm your account", confirmationLetter);
                    UpdatedEmail = true;
                }
                else
                    UpdatedEmail = false;
                if (!await _personalInfoManager.ChangeNameAsync(User, firstName, lastName))
                    throw new Exception();
                if (avatarName != "")
                {
                    if (!await _personalInfoManager.ChangeAvatarAsync(User, avatarName))
                        throw new Exception();
                }
                UpdateResultSuccess = true;
            }
            catch (Exception)
            {
                UpdateResultSuccess = false;
            }
            finally
            {
                LastName = lastName;
                FirstName = firstName;
                Email = email;
                if (avatarName != "") AvatarName = avatarName;
            }
        }
    }
}
