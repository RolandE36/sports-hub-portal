﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using DAL.EF;
using sports_hub_portal.Views.Shared.Components.Comments;
using DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using BAL.Managers;
using BAL.Interfaces;

namespace sports_hub_portal.Components
{
    public class CommentsViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public ApplicationContext _applicationContext;
        private readonly ICommentsManager _commentsManager;
        public CommentsViewComponent(ApplicationContext applicationContext, UserManager<ApplicationUser> userManager, ICommentsManager commentsManager) {
            _applicationContext = applicationContext;
            _userManager = userManager;
            _commentsManager = commentsManager;
        }
        
        [AllowAnonymous]
        public async Task<IViewComponentResult> InvokeAsync(int articleId, string sortType = "Recent")
        {
            var currentUser = await _userManager.GetUserAsync((ClaimsPrincipal)User);
            UserProfile userProfile;
            if (currentUser != null)
            {
                userProfile = await _applicationContext.UserProfiles.FindAsync(currentUser.Id);
            }
            else userProfile = null;
            var comments = _commentsManager.GetComments(articleId);
            var obj = new SectionModel()
            {
                currentUser = userProfile,
                comments = _commentsManager.SortComments(sortType, comments),
                replies = comments.Where(x => x.RepliedCommentID != null).ToList(),
                articleId = articleId,
                sortType = sortType
            };
            return View(obj);
        }
    }
}
