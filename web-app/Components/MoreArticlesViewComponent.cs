using Microsoft.AspNetCore.Mvc;
using DAL.EF;
using Microsoft.AspNetCore.Authorization;
using sports_hub_portal.Views.Shared.Components.MoreArticles;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;

namespace sports_hub_portal.Components
{
    public class MoreArticlesViewComponent : ViewComponent
    {
        ApplicationContext db;
        public MoreArticlesViewComponent(ApplicationContext context)
        {
            db = context;
        }

        [AllowAnonymous]
        public IViewComponentResult Invoke(int categoryId)
        {
            var model = new MoreArticlesModel() { Articles = db.Articles.Include(k => k.Image).Where(x => x.CategoryId == categoryId).Take(Math.Min(6, db.Articles.Where(x => x.CategoryId == categoryId).Count())).ToList() };
            return View(model);
        }
    }
}
