﻿using DAL.Models;
using System.Collections.Generic;

namespace BAL.Interfaces
{
    public interface ICommentsManager
    {
        public List<Comment> GetComments(int articleId);
        public List<CommentRating> GetCommentRatings(int commentId);
        public List<Comment> SortComments(string sortType, List<Comment> comments);
        public void PostComment(string userId, string text, int? commentToChangeId, int articleId, int? repliedCommentId);
        public void DeleteComment(int commentId, int articleId);
        public void RateComment(int commentId, bool isLike, string userId);
    }
}
