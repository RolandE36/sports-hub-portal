﻿using BAL.DTOs.Category;
using DAL.Models;
using System.Collections.Generic;

namespace BAL.Interfaces
{
    public interface ICategoriesManager
    {
        public List<Category> GetAllCategories();
        public void AddCategory(string categoryName, int? parentId);
        public void ChangeVisibility(int id, bool changeTo);
        public List<CategoryDTO> GetCategories();
        public List<CategoryDTO> GetSubcategoriesOfCategory(int categoryId);
    }
}
