﻿using System.Collections.Generic;

using DAL.Models;

namespace BAL.Interfaces
{
    public interface IArticleManager
    {
        public Article GetArticle(int id);
        public List<Article> GetArticleList();
    }
}
