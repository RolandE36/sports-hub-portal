﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BAL.Services
{
    public interface ISaveImageManager
    {
        public string SaveImage(IFormFile image, string folderName);
    }
}
