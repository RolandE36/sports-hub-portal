﻿using BAL.DTOs;
using BAL.DTOs.Partner;
using BAL.DTOs.Teams;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Interfaces
{
    public interface INewsPartnerManager
    {
        public bool AddNewsPartner(string Name);
        public List<NewsPartnerDTO> GetNewsPartners();
        public bool EditPartner(int id, string token, string sources, List<int> enabledCategories, Dictionary<int, string> categorySources);
        public bool TogglePartner(int id, bool state);
        public bool DeletePartner(int partnerId);
    }
}
