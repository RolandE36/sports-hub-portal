﻿using BAL.DTOs;
using BAL.DTOs.Teams;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Interfaces
{
    public interface ITeamsManager
    {
        public bool AddTeam(int teamLocation, int teamCategory, int teamSubсategory, string teamName, string teamLogo);
        public bool EditTeam(int teamId, int teamLocation, int teamCategory, int teamSubсategory, string teamName, string teamLogo);
        public bool DeleteTeam(int teamId);
        public TeamsInfoDTO GetTeams(string name, int location, int categoryId, int subcategoryId, int skip, int take, string sortBy, string sortOrder);
        public bool AddTeamLocation(string LocationName, double Longitude, double Latitude);
        public List<TeamLocationsDTO> GetTeamLocations();
    }
}
