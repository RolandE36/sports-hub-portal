﻿using BAL.DTOs;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Interfaces
{
    public interface IPersonalInfoManager
    {
        public Task<OperationResultDTO> GenerateEmailChangeVerificationTokenAsync(ClaimsPrincipal userClaims, string newEmail);
        public Task<IdentityResult> ChangeEmailAsync(string userId, string token);
        public Task<PersonalInfoDTO> GetPersonalAsync(ClaimsPrincipal userClaims);
        public Task<bool> ChangeNameAsync(ClaimsPrincipal userClaims, string newFirstName, string newLastName);
        public Task<bool> ChangeAvatarAsync(ClaimsPrincipal userClaims, string newAvatarName);
    }
}
