﻿using System;
using System.Collections.Generic;
using System.Text;
namespace BAL.DTOs.Category
{
    public class CategoryDTO
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public int? ParentId { get; set; }
        public bool? Visibility { get; set; }
    }
}
