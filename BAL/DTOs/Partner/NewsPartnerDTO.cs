﻿using BAL.DTOs.Category;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.DTOs.Partner
{
    public class CategoryAndSources
    {
        public CategoryDTO category { get; set; }
        public string Sources { get; set; }
        public bool Enabled { get; set; }
    }
    public class NewsPartnerDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
        public string DefaultSources { get; set; }
        public List<CategoryAndSources> CategoriesAndSources { get; set; }
        public bool Enabled { get; set; }
        public bool CanBeEnabled { get {
                if (string.IsNullOrEmpty(Token)) return false;
                if (string.IsNullOrEmpty(DefaultSources)) return false;
                return true;
            }
        }
    }
}
