﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.DTOs
{
    public class PersonalInfoDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string AvatarName { get; set; }
    }
}
