﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.DTOs
{
    public class OperationResultDTO
    {
        public string Token { get; set; }
        public string Massage { get; set; }
        public string UserId { get; set; }
    }
}
