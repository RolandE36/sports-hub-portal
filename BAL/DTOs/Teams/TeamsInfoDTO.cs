﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.DTOs
{
    public class TeamDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SubcategoryName { get; set; }
        public int SubcategoryId { get; set; }
        public string LocationName { get; set; }
        public int LocationId { get; set; }
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
        public string LogoName { get; set; }
        public DateTime AdditionDate { get; set; }
    }
    public class TeamsInfoDTO
    {
        public List<TeamDetails> Details { get; set; }
        public int QuantityOfResults { get; set; }
    }
}
