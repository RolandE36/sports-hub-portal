﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.DTOs.Teams
{
    public class TeamLocationsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
