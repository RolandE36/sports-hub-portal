﻿using System;
using System.Collections.Generic;
using System.Linq;
using BAL.DTOs.Category;
using BAL.Interfaces;
using DAL.EF;
using DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace BAL.Managers
{
    public class CategoriesManager : ICategoriesManager
    {
        private ApplicationContext _applicationContext;
        public CategoriesManager(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }
        public List<Category> GetAllCategories() => 
            _applicationContext.Categories.Include(x => x.Parent).ToList();
        public List<CategoryDTO> GetCategories() 
        {
            var categories = _applicationContext.Categories.Where((cat) => cat.ParentId == null).ToList();
            var DTOs = categories.ConvertAll((cat) => new CategoryDTO {
                Id = cat.Id,
                CategoryName = cat.Name,
                ParentId = cat.ParentId,
                Visibility = cat.Visibility
            });
            return DTOs;
        }
        public List<CategoryDTO> GetSubcategoriesOfCategory(int categoryId) 
        {
            var categories = _applicationContext.Categories.Where((cat) => cat.ParentId == categoryId).ToList();
            var DTOs = categories.ConvertAll((cat) => new CategoryDTO{
                Id = cat.Id,
                CategoryName = cat.Name,
                ParentId = cat.ParentId,
                Visibility = cat.Visibility
            });
            return DTOs;
        }
        public void AddCategory(string categoryName, int? parentId)
        {
            Category obj;
            if (parentId == null)
            {
                obj = new Category { Name = categoryName };
            }
            else
            {
                obj = new Category { Name = categoryName, Parent = _applicationContext.Categories.Find(parentId) };
            }
            _applicationContext.Categories.Add(obj);
            _applicationContext.SaveChanges();
        }
        public void ChangeVisibility(int id, bool changeTo)
        {
            Category categoryToShow = GetAllCategories().Find(c => c.Id == id);
            categoryToShow.Visibility = changeTo;

            _applicationContext.SaveChanges();
        }
    }
}
