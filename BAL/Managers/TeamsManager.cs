﻿using BAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.EF;
using BAL.DTOs;
using System.Linq;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using BAL.DTOs.Teams;
using System.Linq.Expressions;

namespace BAL.Managers
{
    public class TeamsManager : ITeamsManager
    {
        private readonly ApplicationContext _applicationContext;
        public TeamsManager(ApplicationContext applicationContext) 
        {
            _applicationContext = applicationContext;
        }
        private Team SetCategoryAndSubcategory(Team target, int teamCategory, int teamSubсategory) 
        {
            if (teamCategory < 1)
            {
                if (teamSubсategory < 1)
                {
                    return null;
                }
                else if (_applicationContext.Categories.Find(teamSubсategory).ParentId is null)
                {
                    return null;
                }
                else
                {
                    target.SubcategoryId = teamSubсategory;
                    target.CategoryId = (int)_applicationContext.Categories.Find(teamSubсategory).ParentId;
                }
            }
            else
            {
                if (teamSubсategory < 1)
                {
                    if (_applicationContext.Categories.Find(teamCategory).ParentId is null)
                    {
                        target.SubcategoryId = null;
                        target.CategoryId = teamCategory;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    if (_applicationContext.Categories.Find(teamSubсategory).ParentId == teamCategory)
                    {
                        target.SubcategoryId = teamSubсategory;
                        target.CategoryId = teamCategory;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return target;
        }
        public bool AddTeam(int teamLocation, int teamCategory, int teamSubсategory, string teamName, string teamLogo)
        {
            try
            {
                var team = new Team
                {
                    Name = teamName,
                    LogoName = teamLogo,
                    AdditionDate = DateTime.Now,
                    LocationId = teamLocation
                };
                if (SetCategoryAndSubcategory(team, teamCategory, teamSubсategory) is null)
                    return false;
                _applicationContext.Teams.Add(team);
                _applicationContext.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }
        public bool EditTeam(int teamId, int teamLocation, int teamCategory, int teamSubсategory, string teamName, string teamLogo)
        {
            try
            {
                var team = _applicationContext.Teams.Find(teamId);
                if (team is null) return false;
                team.Name = teamName;
                if (SetCategoryAndSubcategory(team, teamCategory, teamSubсategory) is null)
                    return false;
                team.LogoName = teamLogo;
                team.LocationId = teamLocation;
                _applicationContext.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }
        public bool DeleteTeam(int teamId)
        {
            try
            {
                var teamDelete = _applicationContext.Teams.Find(teamId);
                if (teamDelete is null)
                    return false;
                _applicationContext.Entry(teamDelete).State = EntityState.Deleted;
                _applicationContext.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }
        public TeamsInfoDTO GetTeams(string name, int location, int categoryId, int subcategoryId, int skip, int take, string sortBy, string sortOrder) 
        {
            var result = new TeamsInfoDTO();
            bool filterByName = true;
            bool filterByLocation = true;
            bool filterByCategory = true;
            bool filterBySubcategory = true;
            if (string.IsNullOrEmpty(name))
                filterByName = false;
            if (location < 1)
                filterByLocation = false;
            if (categoryId < 1)
                filterByCategory = false;
            if (subcategoryId < 1)
                filterBySubcategory = false;
            var teams = _applicationContext.Teams
                .Where((team) => (!filterByName || team.Name.Contains(name)) && (!filterByLocation || team.LocationId == location) && (!filterByCategory || team.CategoryId == categoryId) && (!filterBySubcategory || team.SubcategoryId == subcategoryId))
                .Include((t) => t.Category)
                .Include((t) => t.Subcategory)
                .Include((t) => t.Location);
            IOrderedQueryable<Team> orderedTeams;
            switch (sortBy)
            {
                case "Name":
                    if (sortOrder == "ascending") 
                        orderedTeams = teams.OrderBy((team) => team.Name);
                    else
                        orderedTeams = teams.OrderByDescending((team) => team.Name);
                    break;
                case "Location":
                    if (sortOrder == "ascending")
                        orderedTeams = teams.OrderBy((team) => team.Location.Name);
                    else
                        orderedTeams = teams.OrderByDescending((team) => team.Location.Name);
                    break;
                case "AdditionDate":
                    if (sortOrder == "ascending")
                        orderedTeams = teams.OrderBy((team) => team.AdditionDate);
                    else
                        orderedTeams = teams.OrderByDescending((team) => team.AdditionDate);
                    break;
                default:
                    orderedTeams = teams.OrderByDescending((team) => team.Id);
                    break;
            }
            var teamsList = orderedTeams.Skip(skip).Take(take).ToList();
            result.QuantityOfResults = _applicationContext.Teams
                .Where((team) => (!filterByName || team.Name.Contains(name)) && (!filterByLocation || team.LocationId == location) && (!filterByCategory || team.CategoryId == categoryId) && (!filterBySubcategory || team.SubcategoryId == subcategoryId))
                .Count();
            result.Details = teamsList.ConvertAll((t) => new TeamDetails
            {
                Id = t.Id,
                AdditionDate = t.AdditionDate,
                LogoName = t.LogoName,
                LocationId = t.LocationId,
                LocationName = t.Location.Name,
                CategoryId = t.CategoryId,
                CategoryName = t.Category.Name,
                SubcategoryId = t.SubcategoryId is null ? 0 : (int)t.SubcategoryId,
                SubcategoryName = t.SubcategoryId is null ? "Any" : t.Subcategory.Name,
                Name = t.Name
            });
            return result;
        }
        public bool AddTeamLocation(string LocationName, double Longitude, double Latitude) 
        {
            try
            {
                if (string.IsNullOrEmpty(LocationName))
                    return false;
                if (Longitude > 180 || Longitude < -180)
                    return false;
                if (Latitude > 80 || Latitude < -80)
                    return false;
                var location = new TeamLocation { Name = LocationName, Latitude = Latitude, Longitude = Longitude };
                _applicationContext.TeamLocations.Add(location);
                _applicationContext.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }
        public List<TeamLocationsDTO> GetTeamLocations() 
        {
            return _applicationContext.TeamLocations.ToList().ConvertAll((x) => new TeamLocationsDTO {
                Id = x.Id,
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                Name = x.Name
            });
        }
    }
}
