﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Identity;
using DAL.Models;
using DAL.EF;
using DAL.Services;
using BAL.DTOs;
using System.Security.Claims;
using BAL.Interfaces;

namespace BAL.Managers
{
    public class PersonalInfoManager: IPersonalInfoManager
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationContext _applicationContext;

        public PersonalInfoManager(UserManager<ApplicationUser> userManager, ApplicationContext applicationContext)
        {
            _userManager = userManager;
            _applicationContext = applicationContext;
        }
        public async Task<OperationResultDTO> GenerateEmailChangeVerificationTokenAsync(ClaimsPrincipal userClaims, string newEmail) 
        {
            newEmail = newEmail.Trim();
            if (string.IsNullOrEmpty(newEmail)) return new OperationResultDTO { Massage = "New email is requeired" };
            try
            {
                string userId = _userManager.GetUserId(userClaims);
                var user = await _userManager.FindByIdAsync(userId);
                if (user is null) return new OperationResultDTO { Massage = "Cant find user with this Id" };
                if (await _userManager.FindByEmailAsync(newEmail) != null) return new OperationResultDTO { Massage = "User with such email already exists" };
                var userProfile = await _applicationContext.UserProfiles.FindAsync(userId);
                userProfile.NonConfirmedEmail = newEmail;
                await _applicationContext.SaveChangesAsync();
                return new OperationResultDTO { Token = await _userManager.GenerateChangeEmailTokenAsync(user, newEmail), Massage = null, UserId = userId };
            }
            catch (Exception) 
            {
                return new OperationResultDTO { Massage = "Uknown error" };
            }
        }
        public async Task<IdentityResult> ChangeEmailAsync(string userId, string token) 
        {
            try
            {
                if (userId == null || token == null)
                {
                    return IdentityResult.Failed();
                }
                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    return IdentityResult.Failed();
                }
                var userProfile = await _applicationContext.UserProfiles.FindAsync(user.Id);
                var result = await _userManager.ChangeEmailAsync(user, userProfile.NonConfirmedEmail, token);
                if (result.Succeeded)
                {
                    await _userManager.SetUserNameAsync(user, userProfile.NonConfirmedEmail);
                    userProfile.EmailAddess = userProfile.NonConfirmedEmail;
                    await _applicationContext.SaveChangesAsync();
                    return IdentityResult.Success;
                }
                else
                    return IdentityResult.Failed();
            }
            catch (Exception) 
            {
                return IdentityResult.Failed();
            }
        }
        public async Task<PersonalInfoDTO> GetPersonalAsync(ClaimsPrincipal userClaims) 
        {
            try
            {
                var user = await _userManager.GetUserAsync(userClaims);
                var userProfile = await _applicationContext.UserProfiles.FindAsync(user.Id);
                return new PersonalInfoDTO { FirstName = userProfile.FirstName, LastName = userProfile.LastName, AvatarName = userProfile.AvatarName, Email = userProfile.EmailAddess };
            }
            catch (Exception) 
            {
                return null;
            }
        }
        public async Task<bool> ChangeNameAsync(ClaimsPrincipal userClaims, string newFirstName, string newLastName) 
        {
            try
            {
                var user = await _userManager.GetUserAsync(userClaims);
                var userProfile = await _applicationContext.UserProfiles.FindAsync(user.Id);
                if (!string.IsNullOrEmpty(newFirstName))
                {
                    if (newFirstName.Any(char.IsDigit)) return false;
                    userProfile.FirstName = newFirstName;
                }
                if (!string.IsNullOrEmpty(newLastName))
                {
                    if (newLastName.Any(char.IsDigit)) return false;
                    userProfile.LastName = newLastName;
                }
                await _applicationContext.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public async Task<bool> ChangeAvatarAsync(ClaimsPrincipal userClaims, string newAvatarName)
        {
            try
            {
                var user = await _userManager.GetUserAsync(userClaims);
                var userProfile = await _applicationContext.UserProfiles.FindAsync(user.Id);
                if (string.IsNullOrEmpty(newAvatarName))
                    return false;
                userProfile.AvatarName = newAvatarName;
                if(await _applicationContext.SaveChangesAsync() > 0)
                    return true;
                return false;                
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
