﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BAL.Services
{
    public class SaveImageManager: ISaveImageManager
    {
        public string SaveImage(IFormFile image, string folderName) 
        {
            try
            {
                string ImageName = Guid.NewGuid().ToString() + image.FileName.Substring(image.FileName.LastIndexOf("."));
                var file = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "img", folderName, ImageName);
                using (var fileStream = new FileStream(file, FileMode.Create))
                {
                    image.CopyTo(fileStream);
                }
                return ImageName;
            }
            catch (Exception) 
            {
                return null;
            }
        }
    }
}
