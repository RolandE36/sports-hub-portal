﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.EntityFrameworkCore;

using DAL.Models;
using DAL.EF;

using BAL.Interfaces;

namespace BAL.Managers
{
    public class ArticleManager : IArticleManager
    {
        private ApplicationContext applicationContext;
        public ArticleManager(ApplicationContext context)
        {
            applicationContext = context;
        }
        public Article GetArticle(int id)
        {
            return applicationContext.Articles.Include(a => a.Category)
                .Include(a => a.Image)
                .ToList().Find(el => el.Id == id);
        }

        public List<Article> GetArticleList()
        {
            return applicationContext.Articles.Include(a => a.Conference)
                .Include(a => a.Team)
                .Include(a => a.Image)
                .ToList();
        }
    }
}
