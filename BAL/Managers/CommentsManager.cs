﻿using System;
using System.Collections.Generic;
using System.Linq;
using BAL.Interfaces;
using DAL.EF;
using DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace BAL.Managers
{
    public class CommentsManager : ICommentsManager
    {
        private ApplicationContext _applicationContext;
        public CommentsManager(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }
        public List<Comment> GetComments(int articleId) =>
            _applicationContext.Comments
                .Include(z => z.UserProfile)
                .Include(k => k.RatingsFromUsers)
                .Where(x => x.ArticleId == articleId).ToList();
        public List<CommentRating> GetCommentRatings(int commentId) =>
            _applicationContext.Comments.Single(x => x.Id == commentId).RatingsFromUsers.ToList();
        public List<Comment> SortComments(string sortType, List<Comment> comments)
        {
            switch (sortType)
            {
                case "Recent":
                    comments = comments.OrderByDescending(y => y.SubmitTime).ToList();
                    break;
                case "Most popular":
                    comments = comments.OrderByDescending(y => y.RatingsFromUsers.Count()).ToList();
                    break;
                case "Oldest":
                    comments = comments.OrderBy(y => y.Id).ToList();
                    break;
            }
            return comments;
        }
        public void PostComment(string userId, string text, int? commentToChangeId, int articleId, int? repliedCommentId)
        {
            text = text == null ? "" : text.Replace("\r\n", "<br />");
            if (commentToChangeId == null)
                AddComment(userId, text, articleId, repliedCommentId);
            else
                EditComment(text, commentToChangeId);
        }
        public void DeleteComment(int commentId, int articleId)
        {
            _applicationContext.Comments.Remove(_applicationContext.Comments.FirstOrDefault(x => x.Id == commentId));
            _applicationContext.SaveChanges();
            RemoveCommentNode(commentId);
        }
        public void RateComment(int commentId, bool isPositive, string userId)
        {
            Comment ratedComment = _applicationContext.Comments.Include(k => k.RatingsFromUsers).Single(x => x.Id == commentId);
            if (!ratedComment.RatingsFromUsers.Any(x => x.UserProfileId == userId))
                ratedComment.RatingsFromUsers.Add(new CommentRating() {isPositive = isPositive, CommentId = commentId, UserProfileId = userId });
            else if (ratedComment.RatingsFromUsers.Single(x => x.UserProfileId == userId).isPositive == isPositive)
                ratedComment.RatingsFromUsers.RemoveAll(x => x.UserProfileId == userId);
            else
                ratedComment.RatingsFromUsers.Single(x => x.UserProfileId == userId).isPositive = isPositive;
            _applicationContext.Comments.Update(ratedComment);
            _applicationContext.SaveChanges();
        }
        private void AddComment(string userId, string text, int articleId, int? repliedCommentId)
        {
            var submitTime = DateTime.Now;
            var newComment = new Comment
            {
                Text = text,
                UserProfileId = userId,
                SubmitTime = submitTime,
                ArticleId = articleId,
                isEdited = false,
                RepliedCommentID = repliedCommentId
            };
            _applicationContext.Comments.Add(newComment);
            _applicationContext.SaveChanges();
        }
        private void EditComment(string text, int? commentToChangeId)
        {
            var commentToChange = _applicationContext.Comments.FirstOrDefault(x => x.Id == commentToChangeId);
            commentToChange.Text = text;
            commentToChange.isEdited = true;
            _applicationContext.Comments.Update(commentToChange);
            _applicationContext.SaveChanges();
        }
        private void RemoveCommentNode(int commentId)
        {
            var reply_amount = _applicationContext.Comments.Count(x => x.RepliedCommentID == commentId);
            while (reply_amount != 0)
            {
                var reply = _applicationContext.Comments.FirstOrDefault(x => x.RepliedCommentID == commentId);
                _applicationContext.Comments.Remove(reply);
                _applicationContext.SaveChanges();
                RemoveCommentNode((int)reply.Id);
                reply_amount--;
            }
        }

    }
}
