﻿using BAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.EF;
using BAL.DTOs;
using System.Linq;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using BAL.DTOs.Teams;
using BAL.DTOs.Partner;
using BAL.DTOs.Category;

namespace BAL.Managers
{
    public class NewsPartnerManager : INewsPartnerManager
    {
        private readonly ApplicationContext _applicationContext;
        public NewsPartnerManager(ApplicationContext applicationContext) 
        {
            _applicationContext = applicationContext;
        }
        public bool AddNewsPartner(string Name) 
        {
            try
            {
                if (string.IsNullOrEmpty(Name)) return false;
                var Partner = new NewsPartner { Name = Name };
                _applicationContext.NewsPartners.Add(Partner);
                _applicationContext.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }
        public List<NewsPartnerDTO> GetNewsPartners() 
        {
            var partners = _applicationContext.NewsPartners
                .Include(p => p.CategoriesAndSources)
                .ThenInclude(cs => cs.Category).ToList();
            var DTOs = partners.ConvertAll<NewsPartnerDTO>((partner) => {
                var DTO = new NewsPartnerDTO {
                    Id = partner.Id,
                    Name = partner.Name,
                    Token = partner.Token,
                    Enabled = partner.Enabled,
                    DefaultSources = partner.DefaultSources
                };
                if (!(partner.CategoriesAndSources is null)) 
                {
                    DTO.CategoriesAndSources = partner.CategoriesAndSources.ConvertAll<CategoryAndSources>((cs) => new CategoryAndSources {
                        category = new CategoryDTO {
                            Id = cs.Category.Id,
                            CategoryName = cs.Category.Name
                        },
                        Enabled = cs.Enabled,
                        Sources = cs.Sources 
                    });
                }
                return DTO;
            });
            return DTOs;
        }
        public bool EditPartner(int id, string token, string sources, List<int> enabledCategories, Dictionary<int, string> categorySources) 
        {
            try
            {
                var partner = _applicationContext.NewsPartners
                    .Include(p => p.CategoriesAndSources)
                    .Where(p => p.Id == id)
                    .ToList()[0];
                partner.CategoriesAndSources.ForEach((x) =>
                {
                    _applicationContext.Entry(x).State = EntityState.Deleted;
                });
                var categoriesAndSources = new List<NewsPartnerCategoryAndSource>();
                foreach (var encat in enabledCategories)
                {
                    categoriesAndSources.Add(new NewsPartnerCategoryAndSource {
                        Enabled = true,
                        Category = _applicationContext.Categories.Find(encat) 
                    });
                }
                foreach (var cs in categorySources)
                {
                    if (!string.IsNullOrEmpty(cs.Value))
                    {
                        var catAS = categoriesAndSources.Find(x => x.Category.Id == cs.Key);
                        if (catAS == null)
                        {
                            categoriesAndSources.Add(new NewsPartnerCategoryAndSource {
                                Category = _applicationContext.Categories.Find(cs.Key),
                                Sources = cs.Value 
                            });
                        }
                        else 
                        {
                            catAS.Sources = cs.Value;
                        }
                    }
                }
                partner.DefaultSources = sources;
                partner.Token = token;
                partner.CategoriesAndSources = categoriesAndSources;
                _applicationContext.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }
        public bool TogglePartner(int id, bool state)
        {
            try
            {
                var partner = _applicationContext.NewsPartners.Find(id);
                if (partner is null) return false;
                partner.Enabled = state;
                _applicationContext.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }
        public bool DeletePartner(int partnerId) 
        {
            try
            {
                var partner = _applicationContext.NewsPartners
                    .Include(p => p.CategoriesAndSources)
                    .Where(p => p.Id == partnerId)
                    .FirstOrDefault();
                partner.CategoriesAndSources.ForEach((x) =>
                {
                    _applicationContext.Entry(x).State = EntityState.Deleted;
                });
                _applicationContext.Entry(partner).State = EntityState.Deleted;
                _applicationContext.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }
    }
}
