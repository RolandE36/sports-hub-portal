﻿using System;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IUserManager UserManager { get; }
        Task SaveAsync();
    }
}
