﻿using System;
using DAL.Models;

namespace DAL.Interfaces
{
    public interface IUserManager : IDisposable
    {
        void Create(UserProfile item);
    }
}
