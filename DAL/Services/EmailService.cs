﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System.IO;
//using System.Net.Mail;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class EmailService
    {
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var login = builder.Build().GetSection("EmailSender").GetSection("Login").Value;
            var password = builder.Build().GetSection("EmailSender").GetSection("Password").Value;

            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Sport Hub Portal", login));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;


            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 465, true);
                await client.AuthenticateAsync(login, password);
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }

        }
    }
}
