﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Services
{
    public interface IUserService
    {
        string GetUserId();
        bool IsAuthenticated();
    }

}