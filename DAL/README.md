# Enitity Framework init

## For Packages

PM> Install-Package Microsoft.EntityFrameworkCore.SqlServer\
PM> Install-Package Microsoft.EntityFrameworkCore.Tools *if migratons do not create* 

## For migrations 

PM> add-migration CreateUserDB

## For DataBase

PM> update-database –verbose
