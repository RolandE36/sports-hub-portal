﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using DAL.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using System.IO;

namespace DAL.EF
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<TeamLocation> TeamLocations { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Conference> Conferences { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<NewsPartner> NewsPartners { get; set; }
        public DbSet<NewsPartnerCategoryAndSource> NewsPartnerCategoriesAndSources { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional : true, reloadOnChange : true);
            var connection = builder.Build().GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            var version = ServerVersion.AutoDetect(connection);
            optionsBuilder.UseMySql(connection, version);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<CommentRating>()
                .HasKey(x => new { x.CommentId, x.UserProfileId });
            modelBuilder.Entity<CommentRating>()
                .HasOne(x => x.Comment)
                .WithMany(m => m.RatingsFromUsers)
                .HasForeignKey(x => x.CommentId);
            modelBuilder.Entity<CommentRating>()
                .HasOne(x => x.UserProfile)
                .WithMany(e => e.RatedComments)
                .HasForeignKey(x => x.UserProfileId);
            
            modelBuilder.Entity<Category>().HasData(SeedCategories());
        }

        protected List<Category> SeedCategories()
        {
            List<Category> categories = new List<Category>() {
            new Category()
            {
                Id = 1,
                Name = "NBA"

            },
            new Category()
            {
                Id = 2,
                Name = "NFL"

            },
            new Category()
            {
                Id = 3,
                Name = "MLB"

            },
            new Category()
            {
                Id = 4,
                Name = "NHL"

            },
            new Category()
            {
                Id = 5,
                Name = "CBB"

            },
            new Category()
            {
                Id = 6,
                Name = "CFB"

            },
            new Category()
            {
                Id = 7,
                Name = "Nascar"

            },
            new Category()
            {
                Id = 8,
                Name = "Golf"

            },
            new Category()
            {
                Id = 9,
                Name = "Soccer"

            },
            new Category()
            {
                Id = 10,
                Name = "More"

            },
            new Category()
            {
                Id = 11,
                Name = "West",
                ParentId = 1

            },

        };
            return categories;
        }
        
    }
}
