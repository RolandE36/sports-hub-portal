﻿using DAL.EF;
using DAL.Models;
using DAL.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
namespace DAL.Repositories
{
    public class IdentityUnitOfWork : IUnitOfWork
    {
        private ApplicationContext db;

        private IUserManager userManager;

        public IdentityUnitOfWork()
        {
            db = new ApplicationContext();
            userManager = new UserManager(db);
        }

        public IUserManager UserManager
        {
            get { return userManager; }
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    userManager.Dispose();
                }
                this.disposed = true;
            }
        }
    }
}
