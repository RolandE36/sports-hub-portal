﻿using DAL.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
    public class UserProfile
    {
        [Key]
        [ForeignKey("ApplicationUser")]
        public string Id { get; set; }
        public string EmailAddess {get; set;}
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NonConfirmedEmail { get; set; }
        public string AvatarName { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public List<Comment> Comments { get; set; }

        public List<CommentRating> RatedComments { get; set; }
    }
}
