﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Location
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Article> Articles { get; set; }
     }
}
