﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class NewsPartner
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
        public string DefaultSources { get; set; }
        public List<NewsPartnerCategoryAndSource> CategoriesAndSources { get; set; }
        public bool Enabled { get; set; }
    }
}
