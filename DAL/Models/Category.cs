﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using DAL.Models;

namespace DAL.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public List<NewsPartnerCategoryAndSource> PartnersAndSources { get; set; }
        public int? ParentId { get; set; }
        public bool? Visibility { get; set; }
        [JsonIgnore]
        public virtual Category Parent { get; set; }
        [JsonIgnore]
        public virtual ICollection<Category> Children { get; set; }
    }
}