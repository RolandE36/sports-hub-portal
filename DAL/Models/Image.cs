﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Image
    {
        public int Id { get; set; }
        public string Url { get; set; }

        public List<Article> Articles { get; set; }
    }
}
