﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string Alt { get; set; }
        public string Headline { get; set; }
        public string Caption { get; set; }
        public string Content { get; set; }
        public bool Public { get; set; }
        public bool Commentable { get; set; }

        public int ConferenceId { get; set; }
        public Conference Conference { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }


        public int LocationId { get; set; }
        public Location Location { get; set; }

        public int TeamId { get; set; }
        public Team Team { get; set; }

        public int ImageId { get; set; }
        public Image Image { get; set; }
    }
}
