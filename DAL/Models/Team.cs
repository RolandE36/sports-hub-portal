﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DAL.Models;

namespace DAL.Models
{
    public class Team
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string LogoName { get; set; }
        public DateTime AdditionDate { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int? SubcategoryId { get; set; }
        public Category Subcategory { get; set; }
        public List<Article> Articles { get; set; }
        public int LocationId { get; set; }
        public TeamLocation Location { get; set; }
    }
}
