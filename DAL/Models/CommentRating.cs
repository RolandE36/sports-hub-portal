﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class CommentRating
    {
        public bool isPositive { get; set; }

        public int CommentId { get; set; }
        public Comment Comment { get; set; }

        public string UserProfileId { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}
