﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class NewsPartnerCategoryAndSource
    {
        [Key]
        public int Id { get; set; }
        public string Sources { get; set; }
        public bool Enabled { get; set; }
        public NewsPartner NewsPartner { get; set; }
        public Category Category { get; set; }
    }
}
