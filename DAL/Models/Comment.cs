﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public DateTime SubmitTime { get; set; }
        public int ArticleId { get; set; }
        public string Text { get; set; }
        public bool isEdited { get; set; }
        public int? RepliedCommentID { get; set; }

        public string UserProfileId { get; set; }
        public UserProfile UserProfile { get; set; }

        public List<CommentRating> RatingsFromUsers { get; set; } = new List<CommentRating>();
    }
}
