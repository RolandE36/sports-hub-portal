# sports-hub-portal [sportshubportal.duckdns.org](http://sportshubportal.duckdns.org:8080/)

[Project Description](https://github.com/dark-side/lanthanum/tree/master/products/sports_hub_portal)

## Stories Progress:
1. [ ] Log in and sign up to the portal
    1. [x] #18 Allow users to sign up on the portal using an email @Denus456
        1. [x] #1 Login page - Front-end only (HTML/CSS)
        1. [x] #9 Create ApplicationUser Models
        1. [x] #10 Create Account
    1. [x] #19 Allow users to log in to the portal using an email  @Denus456
    1. [ ] Allow users to sign up using a third-party authentication provider @Denus456	
    1. [ ] Allow users to log in to the portal using a third-party authentication provider	
    1. [x] Allow users to reset their password @Denus456
    1. [x] #3 Allow users to update their personal information @abrikoskokos42	
    1. [x] #20 Allow users to change their password via profile @VladyslavkoL
1. [ ] Page layout on the portal
    1. [ ] Page structure (user side)
        1. [x] #2 Layout page @romankhmil0705
    1. [ ] Page structure (admin side)
1. [ ] Site footer of the portal
1. [ ] Maintain navigation on the portal
    1. [ ] #15 Allow admin user to create navigation menu items on the portal @nataliiadivchur	
    1. [ ] Allow admin user to show and hide navigation menu items on the portal	
    1. [ ] Allow admin user to change order of the navigation menu items and move them between the categories and subcategories	
    1. [ ] Allow site users to navigate to the sports categories, subcategories, and teams page on the portal
        1. [x] #4 Left side navigation pane @nataliiadivchur
1. [ ] Manage teams on the portal
1. [ ] Social networks on the portal
1. [ ] Site languages on the portal
1. [ ] Manage articles on the portal
    1. [ ] #14 Allow admin users to add a new article @romankhmil0705
    1. [ ] Allow admin users to view the list of existing articles	
    1. [ ] Allow admin users to edit an existing article	
    1. [ ] Allow admin users to delete an existing article	
    1. [ ] Allow admin users to publish and unpublish an existing article	
    1. [ ] Allow admin users to move an existing article between categories	
    1. [ ] Allow admin users to have advanced editing of the article image
1. [ ] Articles - user side on the portal
    1. [ ] #6 Allow users to view an article on the portal @sophiychuk.v.a
    1. [ ] Allow users to share an article from the portal	
    1. [ ] #5 Allow users to view More Articles block on the portal @Scripa	
    1. [ ] Allow users to view league/team page of the portal
1. [ ] Home page of the portal
1. [ ] Lifestyle and Dealbook news
1. [ ] Sports videos on the portal
1. [ ] The most popular and commented articles on the portal
1. [ ] Manage news partners on the portal
1. [ ] Comments block on the portal @Scripa
1. [ ] Global site search on the portal
1. [ ] Newsletter subscription
1. [ ] Team hub: news about favorite teams
1. [ ] User management on the portal
1. [ ] Banners on the portal
1. [ ] Manage advertisements on the portal
1. [ ] Surveys on the portal
1. [ ] Dev Stories
    1. [x] #8 Add EF @VladyslavkoL
    1. [ ] #11 Create User personas (Roles) @sophiychuk.v.a
    1. [x] #21 EF Pomelo @Scripa
    1. [x] #16 Images upload functionality @abrikoskokos42



#16 Allow users to comment an article on the portal

#14 Allow admin users to add a new article @romankhmil0705

## Developers
Denus Kotovcky
@Denus456

Mykola Konopelskyi
@abrikoskokos42

Nataliia Divchur
@nataliiadivchur

Roman Khmil
@romankhmil0705

Roman Skrypchuk
@Scripa

Vladyslav Leskiv
@VladyslavkoL

Volodymyr Sophiychuk
@sophiychuk.v.a



